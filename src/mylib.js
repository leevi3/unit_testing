/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function. */
    add: (a, b) =>{
        const sum = a + b;
        return sum;
    },
    substract: (a, b) =>{
        return a-b;
    },
    /** Singleline arrow function. */
    divide: (divident, divisor) => divident / divisor,
    /** Regular function. */
    multiply: function(a, b) {
        return a*b;
    }
};

module.exports = mylib;