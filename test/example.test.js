//** imports */
const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before (() => {
        // initialization
        // create objects.. etc..
        console.log("Initializing completed.");
    });
    it("1+2", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason");
    });
    it("1-2", () => {
        // Tests
        expect(mylib.substract(1,2)).equal(-1, "1 - 2 is not -1, for some reason");
    });
    it("1/2", () => {
        // Tests
        expect(mylib.divide(1,2)).equal(0.5, "1 / 2 is not 0.5, for some reason");
    });
    it("1*2", () => {
        // Tests
        expect(mylib.multiply(1,2)).equal(2, "1 * 2 is not 2, for some reason");
    });
    it("1/0 = error", () => {
        // Tests
        expect(mylib.divide(1,0)).equal(Infinity, "1 / 0 works for some reason");
    });
    after (() => {
        //Cleanup
        //shut express server down
        console.log("Testing completed")
    });
});
